char* getResponse(SSL **httpsSock){
	char response[4096] = {0};
	int readlen = SSL_read(*httpsSock, response, 4095);

	if(readlen < 1){
		printf("Failed to read response headers!\n");
		return NULL;
	}

	//I'm going to assume that 4095 bytes is enough to get the response headers at least...
	char *conLen = strstr(response, "Content-Length: ");
	unsigned long contentLength = 0;

	if(conLen == NULL){
		conLen = strstr(response, "\r\n\r\n");

		if(conLen == NULL){
			printf("Unable to find length of content\n");
			return NULL;
		}

		contentLength = strtoul(conLen + 4, NULL, 16);
	}else
		contentLength = strtoul(conLen + 16, NULL, 10);

	printf("length: %lu\nresponse\n%s", contentLength, response);

	while(SSL_pending(*httpsSock) != 0){
		SSL_read(*httpsSock, response, 4095);
		printf("%s", response);
		memset(response, 0, 4095);
	}

	printf("\n");

	return NULL;
}

void getChannel(struct apiObj *apiObj, char *channelId){
	int httpSock;
	SSL *httpsSock;
	SSL_CTX *httpsCTX;

	if(!openSocks(apiObj->urlHost, &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Failed to open connection to %s\n", apiObj->urlHost);
		return;
	}

	char request[4096] = {0};
	sprintf(request, "GET %schannels/%s %s\r\n", apiObj->urlPath, channelId, apiObj->requiredHeaders);

	SSL_write_lock(httpsSock, request, strlen(request));

	char *response = getResponse(&httpsSock);
	if(response != NULL)
		printf("Response: %s\n", response);

	closeSocks(&httpSock, &httpsSock, &httpsCTX);
}

void modifyChannel(struct apiObj *apiObj, char *channelId, char *key, char *val){
	int httpSock;
	SSL *httpsSock;
	SSL_CTX *httpsCTX;

	if(!openSocks(apiObj->urlHost, &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Failed to open connection to %s\n", apiObj->urlHost);
		return;
	}

	char request[4096] = {0};
	char *eKey = JSONEscape(key), *hKey;
	if(eKey == NULL)
		hKey = key;
	else
		hKey = eKey;

	char *eVal = JSONEscape(val), *hVal;
	if(eVal == NULL)
		hVal = val;
	else
		hVal = eVal;


	sprintf(request, "PATCH %schannels/%s %sContent-Type: application/json\r\nContent-Length: %zu\r\n\r\n{\"%s\":\"%s\"}", apiObj->urlPath, channelId, apiObj->requiredHeaders, strlen(hKey) + strlen(hVal) + 7, hKey, hVal);
	SSL_write_lock(httpsSock, request, strlen(request));

	if(eKey != NULL)
		free(eKey);
	
	if(eVal != NULL)
		free(eVal);

	char *response = getResponse(&httpsSock);
	if(response != NULL)
		printf("Response: %s\n", response);

	closeSocks(&httpSock, &httpsSock, &httpsCTX);
}

void deleteChannel(struct apiObj *apiObj, char* channelId){
	int httpSock;
	SSL *httpsSock;
	SSL_CTX *httpsCTX;

	if(!openSocks(apiObj->urlHost, &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Failed to open connection to %s\n", apiObj->urlHost);
		return;
	}

	char request[4096] = {0};
	sprintf(request, "DELETE %schannels/%s %s\r\n", apiObj->urlPath, channelId, apiObj->requiredHeaders);
	SSL_write_lock(httpsSock, request, strlen(request));

	char *response = getResponse(&httpsSock);
	if(response != NULL)
		printf("Response: %s\n", response);

	closeSocks(&httpSock, &httpsSock, &httpsCTX);
}

void getChannelMessages(struct apiObj *apiObj, char* channelId, unsigned char limit, unsigned char mode, char* messageId){
	int httpSock;
	SSL *httpsSock;
	SSL_CTX *httpsCTX;

	if(!openSocks(apiObj->urlHost, &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Failed to open connection to %s\n", apiObj->urlHost);
		return;
	}

	if(limit == 0 || limit > 100)
		limit = 50;

	char request[4096] = {0};
	sprintf(request, "GET %schannels/%s/messages?limit=%hhu", apiObj->urlPath, channelId, limit);

	printf("message ID: %s\n", messageId);

	switch(mode){
		case GET_CHANNEL_MESSAGES_INDEX_MODE_AROUND:
			printf("Around!\n");
		break;
		case GET_CHANNEL_MESSAGES_INDEX_MODE_BEFORE:
			printf("Before!\n");
		break;
		case GET_CHANNEL_MESSAGES_INDEX_MODE_AFTER:
			printf("After!\n");
		break;
		default:
			printf("END!\n");
		break;
	}

	strcat(request, " ");
	strcat(request, apiObj->requiredHeaders);
	strcat(request, "\r\n");

	printf("request: %s\n", request);

	SSL_write_lock(httpsSock, request, strlen(request));

	char *response = getResponse(&httpsSock);
	if(response != NULL)
		printf("Response: %s\n", response);

	closeSocks(&httpSock, &httpsSock, &httpsCTX);
}

void createMessage(struct apiObj *apiObj, char *channelId, char *message){
	int httpSock;
	SSL *httpsSock;
	SSL_CTX *httpsCTX;

	if(!openSocks(apiObj->urlHost, &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Failed to open connection to %s\n", apiObj->urlHost);
		return;
	}

	char request[4096] = {0};
	char *eMessage = JSONEscape(message), *hMessage;
	if(eMessage == NULL)
		hMessage = message;
	else
		hMessage = eMessage;

	sprintf(request, "POST %schannels/%s/messages %sContent-Type: application/json\r\nContent-Length: %zu\r\n\r\n{\"content\":\"%s\"}", apiObj->urlPath, channelId, apiObj->requiredHeaders, strlen(hMessage) + 14, hMessage);
	SSL_write_lock(httpsSock, request, strlen(request));

	if(eMessage != NULL)
		free(eMessage);

	getResponse(&httpsSock);
	closeSocks(&httpSock, &httpsSock, &httpsCTX);
}
