#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <poll.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>


// OpenSSL doesn't thread well apparently as of 1.0.2... Will supposed get better in 1.1.0... We'll see

enum identityType{
	IDENTITY_NODE_TYPE_BOT,
	IDENTITY_NODE_TYPE_SELF
};

struct identityNode{
	char *name, *token, *email, *password;
	unsigned char type;
	unsigned int hbInterval;
	int wsSock;
	SSL *wssSock;
	SSL_CTX *wssCTX;
	struct identityNode *next;
} *iRoot = NULL;

struct moduleNode{
	char *name;
	void *module;
	void* (*closeFunc)();
	struct moduleNode *next;
} *mRoot = NULL;

struct commandNode{
	char *command;
	unsigned char len;
	char *description;
	void* (*func)();
	struct commandNode *next;
} *cRoot = NULL;

struct wshNode{
	char *name;
	void* (*func)();
	struct wshNode *next;
} *wshRoot = NULL;

#include "apiObj.h"
#include "funcs.c"
#include "apiFuncs.c"


int main(){
	// This is imperfect, but probably good enough for this purpose...
	srand(time(NULL));

	// Start up OpenSSL
	OpenSSL_add_all_algorithms();
	ERR_load_BIO_strings();
	ERR_load_crypto_strings();
	SSL_load_error_strings();

	int sslI = SSL_library_init();
	if(sslI < 0){
		printf("SSL Init failed: %i\n", sslI);
		return 0;
	}

	pthread_t hbThread, sockThread;

	pthread_create(&hbThread, NULL, hbThreadFunc, NULL);
	pthread_detach(hbThread);

	pthread_create(&sockThread, NULL, sockThreadFunc, NULL);
	pthread_detach(sockThread);

	apiObj = malloc(sizeof(struct apiObj));

	apiObj->urlHost = "discordapp.com";
	apiObj->urlPath = "/api/";
	apiObj->requiredHeaders = "HTTP/1.1\r\nHost: discordapp.com\r\nUser-Agent: CBot ( , -1)\r\n";

	// Authorization: Bot "BOTTOKEN"\r\n

	if(
		pthread_mutex_init(&(apiObj->sslLock), NULL) != 0
	 || pthread_mutex_init(&(apiObj->iLock), NULL) != 0
	 || pthread_mutex_init(&(apiObj->cLock), NULL) != 0
	 || pthread_mutex_init(&(apiObj->wshLock), NULL) != 0
	){
		printf("mutex init failed!\n");
		return 0;
	}

	apiObj->addWSH = &addWSH;
	apiObj->delWSH = &delWSH;

	apiObj->addCommand = &addCommand;
	apiObj->delCommand = &delCommand;


	apiObj->getChannel = &getChannel;
	apiObj->modifyChannel = &modifyChannel;
	apiObj->deleteChannel = &deleteChannel;
	apiObj->createMessage = &createMessage;
	apiObj->getChannelMessages = &getChannelMessages;

	// See if there are any saved credentials... If so, load them up
	size_t length;
	char *msg = NULL;
	FILE *f = fopen("credentials.dat", "a+"), *ft = fopen("tmp-credentials.dat", "w");

	if(f == NULL || ft == NULL){
		printf("Failed to read or create credentials files! %i\n", errno);
		return 0;
	}

	if(!feof(f)){
		iRoot = malloc(sizeof(struct identityNode));
		struct identityNode *prev = NULL, *cur = iRoot;
		int readLen;

		while((readLen = getline(&msg, &length, f)) > -1){
			if(!strlen(msg)){
				free(msg);
				msg = NULL;
				continue;
			}

			char *tmp = msg;

			cur->name = malloc(strlen(tmp) + 1);
			strcpy(cur->name, tmp);
			tmp += strlen(tmp) + 1;
			
			cur->type = tmp[0];
			
			tmp += 2;

			switch(cur->type){
			case IDENTITY_NODE_TYPE_BOT:
				cur->token = malloc(strlen(tmp) + 1);
				strcpy(cur->token, tmp);

				cur->email = NULL;
				cur->password = NULL;
			break;
			case IDENTITY_NODE_TYPE_SELF:
				cur->email = malloc(strlen(tmp) + 1);
				strcpy(cur->email, tmp);

				tmp += strlen(tmp) + 1;
				cur->password = malloc(strlen(tmp) + 1);
				strcpy(cur->password, tmp);

				cur->token = NULL;
			}

			prev = cur;
			cur->next = malloc(sizeof(struct identityNode));
			cur = cur->next;

			fwrite(msg, 1, readLen, ft);

			free(msg);
			msg = NULL;
		}

		if(prev == NULL)
			iRoot = NULL;
		else
			prev->next = NULL;

		free(cur);
	}

	fclose(f);
	fclose(ft);

	if(rename("tmp-credentials.dat", "credentials.dat") != 0){
		printf("Failed to update \"credentials.dat\"! %i\n", errno);
		return 0;
	}

	// Start console loop
	printf("For a list of commands, type help\n\nCBot> ");

	while(getline(&msg, &length, stdin) > -1){
		size_t strLen = strlen(msg);
		if(strLen == 1)
			goto endAndFree;

		msg[--strLen] = 0;

		if(!strcasecmp(msg, "help"))
			printf("Help here\n");

		else if(!strcmp(msg, "add account")){
			struct identityNode *iNode = malloc(sizeof(struct identityNode));

			inputEntry:

			printf("\nType of account?\n - Bot\n - Self\n\n");

			free(msg);
			msg = NULL;
			if(getline(&msg, &length, stdin) < 0){
				printf("Error reading input...\n");
				goto endAndFree;
			}

			if(!strcasecmp(msg, "bot\n"))
				iNode->type = IDENTITY_NODE_TYPE_BOT;
			else if(!strcasecmp(msg, "self\n"))
				iNode->type = IDENTITY_NODE_TYPE_SELF;
			else{
				printf("Type not recognized...\n");
				goto inputEntry;
			}

			iNode->wsSock = 0;
			iNode->wssSock = NULL;
			iNode->wssCTX = NULL;
			iNode->next = NULL;

			f = fopen("credentials.dat", "a+");

			switch(iNode->type){
			case IDENTITY_NODE_TYPE_BOT:
				botEnterName:
				free(msg);
				msg = NULL;

				printf("\nEnter a name for this bot: ");
				if(getline(&msg, &length, stdin) < 0){
					printf("Input error\n");
					free(iNode);
					fclose(f);
					goto endAndFree;
				}

				strLen = strlen(msg);
				if(strLen < 2){
					printf("no name was entered.\n");
					goto botEnterName;
				}

				msg[strLen-1] = 0;
				iNode->name = malloc(strLen);
				strcpy(iNode->name, msg);

				printf("\n");

				botEnterToken:
				free(msg);
				msg = NULL;

				printf("Enter your token: (Nothing will display)\n");
				if(getline(&msg, &length, stdin) < 0){
					printf("Input error\n");
					free(iNode);
					fclose(f);
					goto endAndFree;
				}

				strLen = strlen(msg);
				if(strLen < 2){
					printf("No token was entered.\n");
					goto botEnterToken;
				}

				msg[strLen-1] = 0;
				iNode->token = malloc(strLen);
				strcpy(iNode->token, msg);

				iNode->email = NULL;
				iNode->password = NULL;

				fputs(iNode->name, f);
				fputc(0, f);
				fputc(iNode->type, f);
				fputc(0, f);
				fputs(iNode->token, f);
				fputc(0, f);
				fputc('\n', f);
			break;
			case IDENTITY_NODE_TYPE_SELF:
				printf("Note that self bots are against the Discord TOS. Use at your own risk!\n\n");

				selfEnterName:
				free(msg);
				msg = NULL;

				printf("\nEnter a name for this self bot: ");
				if(getline(&msg, &length, stdin) < 0){
					printf("Input error\n");
					free(iNode);
					fclose(f);
					goto endAndFree;
				}

				strLen = strlen(msg);
				if(strLen < 2){
					printf("No name was entered.\n");
					goto selfEnterName;
				}

				msg[strLen-1] = 0;
				iNode->name = malloc(strLen);
				strcpy(iNode->name, msg);

				printf("\n");

				selfEnterEmail:
				free(msg);
				msg = NULL;

				printf("Enter your email:\n");
				if(getline(&msg, &length, stdin) < 0){
					free(iNode);
					fclose(f);
					goto endAndFree;
				}

				strLen = strlen(msg);
				if(strLen < 2){
					printf("No email was entered.\n");
					goto selfEnterEmail;
				}

				msg[strLen-1] = 0;
				iNode->email = malloc(strLen);
				strcpy(iNode->email, msg);

				selfEnterPassword:
				free(msg);
				msg = NULL;

				printf("Enter your password: (Nothing will display)\n");
				if(getline(&msg, &length, stdin) < 0){
					free(iNode);
					fclose(f);
					goto endAndFree;
				}

				strLen = strlen(msg);
				if(strLen < 2){
					printf("Nothing was entered.\n");
					goto selfEnterPassword;
				}

				msg[strLen-1] = 0;
				iNode->password = malloc(strLen);
				strcpy(iNode->password, msg);

				iNode->token = NULL;

				fputs(iNode->name, f);
				fputc(0, f);
				fputc(iNode->type, f);
				fputc(0, f);
				fputs(iNode->email, f);
				fputc(0, f);
				fputs(iNode->password, f);
				fputc(0, f);
				fputc('\n', f);
			}

			fclose(f);

			pthread_mutex_lock(&(apiObj->iLock));
			if(iRoot == NULL){
				iRoot = iNode;
				pthread_mutex_unlock(&(apiObj->iLock));
				goto endAndFree;
			}

			struct identityNode *tmpNode = iRoot;

			while(tmpNode->next != NULL)
				tmpNode = tmpNode->next;

			tmpNode->next = iNode;
			pthread_mutex_unlock(&(apiObj->iLock));
		}else if(!strncmp(msg, "remove account ", 15)){
			pthread_mutex_lock(&(apiObj->iLock));
			struct identityNode *iNode = iRoot, *prev = NULL;
			while(iNode != NULL && strcmp(msg+15, iNode->name)){
				prev = iNode;
				iNode = iNode->next;
			}

			if(iNode == NULL){
				printf("Account \"%s\" not found!\n", msg+15);
				goto endAndFree;
			}

			if(iNode->wssSock != NULL){
				printf("Account \"%s\" must be disconnected before it can be removed!\n", msg+15);
				goto endAndFree;
			}


			if(prev == NULL)
				iRoot = iNode->next;
			else
				prev->next = iNode->next;

			switch(iNode->type){
			case IDENTITY_NODE_TYPE_BOT:
				free(iNode->token);
			break;
			case IDENTITY_NODE_TYPE_SELF:
				free(iNode->email);
				free(iNode->password);
			}

			printf("Removed account \"%s\"\n", iNode->name);

			char *name = iNode->name;

			free(iNode);
			pthread_mutex_unlock(&(apiObj->iLock));

			f = fopen("credentials.dat", "r+");
			free(msg);
			msg = NULL;
			ssize_t readLen;
			while((readLen = getline(&msg, &length, f)) > -1 && strcmp(msg, iNode->name)){
				free(msg);
				msg = NULL;
			}

			// Actually removing a string in the middle of a file is a lot of work... Let's just over-write the line with NULL bytes and clean it up on next start up.
			// Will make this instant eventually...
			fseek(f, readLen * -1, SEEK_CUR);
			while(--readLen > 0)
				fputc(0, f);

			fclose(f);
			free(name);

		}else if(!strcmp(msg, "accounts")){
			pthread_mutex_lock(&(apiObj->iLock));
			struct identityNode *iNode = iRoot;
			printf("Accounts:\n");

			while(iNode != NULL){
				printf(" - ");
				
				switch(iNode->type){
				case IDENTITY_NODE_TYPE_BOT:
					printf("B");
				break;
				case IDENTITY_NODE_TYPE_SELF:
					printf("S");
				}

				if(iNode->hbInterval != 0)
					printf("C");

				printf(" %s\n", iNode->name);
				iNode = iNode->next;
			}
			pthread_mutex_unlock(&(apiObj->iLock));

		}else if(!strncmp(msg, "connect ", 8)){
			struct identityNode *iNode = iRoot;
			while(iNode != NULL && strcmp(msg+8, iNode->name))
				iNode = iNode->next;

			if(iNode == NULL)
				printf("Account \"%s\" not found!\n", msg+8);
			else
				connectToDiscord(iNode);
		}else if(!strncmp(msg, "disconnect ", 11)){
			struct identityNode *iNode = iRoot;
			while(iNode != NULL && strcmp(msg+11, iNode->name))
				iNode = iNode->next;

			if(iNode == NULL){
				printf("Account \"%s\" not found!\n", msg+11);
				goto endAndFree;
			}

			if(iNode->wssSock == NULL){
				printf("Account \"%s\" is already disconnected\n", msg+11);
				goto endAndFree;
			}

			iNode->hbInterval = 0;

			closeSocks(&(iNode->wsSock), &(iNode->wssSock), &(iNode->wssCTX));
			printf("Disconnected\n");
		}else if(!strcasecmp(msg, "loaded")){
			printf("Loaded modules:\n");

			struct moduleNode *tmpNode = mRoot;
			while(tmpNode != NULL){
				printf(" - %s\n", tmpNode->name);
				tmpNode = tmpNode->next;
			}

		}else if(!strncasecmp(msg, "load ", 5)){
			char *name = malloc(strLen + 3);
			sprintf(name, "bin/%s.so", msg + 5);

			void *module = dlopen(name, RTLD_NOW);

			if(!module){
				printf("Module \"%s\" not found!\n", name);
				free(name);
				goto endAndFree;
			}

			void* (*onOpenFunc)();
			void* (*onCloseFunc)();
			*(void**)(&onOpenFunc) = dlsym(module, "onOpenFunc");
			*(void**)(&onCloseFunc) = dlsym(module, "onCloseFunc");

			if(onOpenFunc != NULL)
				onOpenFunc(apiObj);

			struct moduleNode *node = malloc(sizeof(struct moduleNode));
			node->name = name;
			node->module = module;
			node->closeFunc = onCloseFunc;
			node->next = NULL;

			if(mRoot == NULL)
				mRoot = node;

			else{
				struct moduleNode *tmpNode = mRoot;
				while(tmpNode->next != NULL)
					tmpNode = tmpNode->next;

				tmpNode->next = node;
			}

			printf("Module \"%s\" has been loaded\n", name);

		}else if(!strncasecmp(msg, "unload ", 7)){
			struct moduleNode *tmpNode = mRoot, *nextNode = NULL, *prevNode = NULL;
			char *name = malloc(strLen + 1);
			sprintf(name, "bin/%s.so", msg + 7);

			while(tmpNode != NULL && strcmp(tmpNode->name, name) != 0){
				prevNode = tmpNode;
				tmpNode = tmpNode->next;
			}

			if(tmpNode == NULL){
				printf("Module \"%s\" is not loaded\n", name);
				free(name);
				goto endAndFree;
			}

			if(tmpNode->closeFunc != NULL)
				tmpNode->closeFunc();

			nextNode = tmpNode->next;
			free(tmpNode->name);
			dlclose(tmpNode->module);
			free(tmpNode);

			if(prevNode == NULL)
				mRoot = nextNode;
			else
				prevNode->next = nextNode;

			printf("Unloaded module \"%s\"\n", name);
			free(name);

		}else if(!strcmp(msg, "quit"))
			return 0;

		else{
			struct commandNode *node = cRoot;
			while(node != NULL && ((!node->len && strcmp(node->command, msg) != 0) || (node->len != 0 && strncmp(node->command, msg, node->len))))
				node = node->next;

			if(node == NULL)
				printf("Command \"%s\" not found\n", msg);
			else
				node->func(msg);
		}

		endAndFree:

		printf("\nCBot> ");
		free(msg);
		msg = NULL;
	}

	return 0;
}
