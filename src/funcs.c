void addCommand(char *command, unsigned char len, char *description, void *func){
	struct commandNode *node = malloc(sizeof(struct commandNode));

	node->command = malloc(strlen(command) + 1);
	strcpy(node->command, command);

	node->description = malloc(strlen(description) + 1);
	strcpy(node->description, description);

	void *(*fun)();
	*(void**)(&fun) = func;
	node->func = fun;
	node->len = len;
	node->next = NULL;

	if(cRoot == NULL)
		cRoot = node;

	else{
		struct commandNode *tmpNode = cRoot;
		while(tmpNode->next != NULL)
			tmpNode = tmpNode->next;

		tmpNode->next = node;
	}
}

void delCommand(char *name){
	struct commandNode *node = cRoot, *prevNode = NULL, *nextNode = NULL;

	while(node != NULL && !strcmp(node->command, name)){
		prevNode = node;
		node = node->next;
	}
	
	if(node == NULL)
		return;
	
	nextNode = node->next;

	free(node->command);
	free(node->description);
	free(node);

	if(prevNode != NULL)
		prevNode->next = nextNode;
	else
		cRoot = nextNode;
}

void addWSH(char *name, void *func){
	pthread_mutex_lock(&(apiObj->wshLock));
	struct wshNode *node = malloc(sizeof(struct wshNode));

	node->name = malloc(strlen(name) + 1);
	strcpy(node->name, name);

	void *(*fun)();
	*(void**)(&fun) = func;
	node->func = fun;
	node->next = NULL;

	if(wshRoot == NULL)
		wshRoot = node;

	else{
		struct wshNode *tmpNode = wshRoot;
		while(tmpNode->next != NULL)
			tmpNode = tmpNode->next;

		tmpNode->next = node;
	}
	pthread_mutex_unlock(&(apiObj->wshLock));
}

void delWSH(char *name){
	pthread_mutex_lock(&(apiObj->wshLock));
	struct wshNode *node = wshRoot, *prevNode = NULL, *nextNode = NULL;

	while(node != NULL && strcmp(node->name, name) != 0){
		prevNode = node;
		node = node->next;
	}
	
	if(node == NULL)
		return;
	
	nextNode = node->next;

	free(node->name);
	free(node);

	if(prevNode != NULL)
		prevNode->next = nextNode;
	else
		wshRoot = nextNode;

	pthread_mutex_unlock(&(apiObj->wshLock));
}

// This DOES NOT handle unicode yet... May break unicode
char* JSONEscape(char *content){
	unsigned long offset = 0, maxResLen = strlen(content) * 2 + 1;
	char *result = malloc(maxResLen), *last,*tmp;

	tmp = strpbrk(content, "\"\\\b\f\n\r\t");
	if(tmp == NULL){
		free(result);
		return NULL;
	}

	offset = tmp - content;
	if(offset != 0)
		memcpy(result, content, offset);

	for(;;){
		memcpy(&result[offset++], "\\", 1);
		last = tmp;
		tmp = strpbrk(tmp+1, "\"\\\b\f\n\r\t");
		if(tmp == NULL)
			break;

		memcpy(&result[offset], last, tmp - last);
		offset += tmp - last;
	}

	// Don't forget to add the 0 byte at the end
	memcpy(&result[offset], last, strlen(last) + 1);
	return realloc(result, strlen(result) + 1);
}

int SSL_write_lock(SSL *ssl, const void *buf, int num){
	pthread_mutex_lock(&(apiObj->sslLock));
	int ret = SSL_write(ssl, buf, num);
	pthread_mutex_unlock(&(apiObj->sslLock));
	return ret;
}

int wsReadMsg(SSL *wssSock, char **msg, unsigned char *opcode){
	pthread_mutex_lock(&(apiObj->sslLock));

	char header[2] = {0};
	uint64_t len = 0;

	int status;

	if((status = SSL_read(wssSock, header, 2)) < 1){
		printf("wsReadMsg error: %i\n", status);
		pthread_mutex_unlock(&(apiObj->sslLock));
		return 0;
	}

	if(header[0] != -127){
		printf("websocket error? %i %i\n", header[0], header[1]);

		if(header[0] == -120 && header[1] == 2){
			SSL_read(wssSock, header, 2);
			printf("Error Code: %hu\n", ((unsigned char)header[0] * 256) + (unsigned char)header[1]);
		}else if(header[1] > 0){
			char buffer[4096] = {0};
			SSL_read(wssSock, buffer, 4095);
			printf("Message: %s\n", buffer);
		}

		pthread_mutex_unlock(&(apiObj->sslLock));
		printf("b\n");
		return 0;
	}

	switch(header[1]){
		case 127:
			;
			union ULength8{
				uint64_t length;
				unsigned char bytes[8];
			} ulength8;

			ulength8.length = 0;

			for(len=7; len!=(uint64_t)-1; len--)
				SSL_read(wssSock, &ulength8.bytes[len], 1);

			len = ulength8.length;
		break;

		case 126:
			;
			union ULength2{
				uint64_t length;
				unsigned char bytes[2];
			} ulength2;

			ulength2.length = 0;

			SSL_read(wssSock, &ulength2.bytes[1], 1);
			SSL_read(wssSock, ulength2.bytes, 1);

			len = ulength2.length;
		break;

		default:
			len = header[1];
	}

	while((uint64_t)SSL_pending(wssSock) < len){
		printf("pending: %u len: %"PRIu64"\n", SSL_pending(wssSock), len);
		if(SSL_pending(wssSock) == 0)
			continue;

		unsigned long pending = SSL_pending(wssSock);

		*msg = malloc(pending + 1);
		SSL_read(wssSock, *msg, pending);
		(*msg)[pending] = 0;
		printf("msg: %s\n", *msg);
		exit(0);
	}

	*msg = malloc(len + 1);
	SSL_read(wssSock, *msg, len);
	(*msg)[len] = 0;

	*opcode = (unsigned char)strtoul(strstr(*msg, "\"op\":") + 5, NULL, 10);

	pthread_mutex_unlock(&(apiObj->sslLock));

	return 1;
}

int wsSendMsg(SSL *wssSock, char *msg){
	pthread_mutex_lock(&(apiObj->sslLock));
	if(wssSock == NULL){
		pthread_mutex_unlock(&(apiObj->sslLock));
		return 0;
	}

	uint64_t length = strlen(msg);

	if(length > 65535){
		printf("Still need to implement sending messages over 65535 bytes in length...\n");
		return 0;
	}

	unsigned char offset = 0;
	char *encoded = NULL; 

	if(length > 125){
		offset = 4;
		encoded = malloc(length + 9);

		encoded[0] = -127;
		encoded[1] = (char)(126 + 128);
		encoded[2] = (unsigned char)(length/256);
		encoded[3] = length % 256;

	}else{
		offset = 2;
		encoded = malloc(length + 7);

		encoded[0] = -127;
		encoded[1] = length + 128;
	}

	uint64_t i;
	for(i=0; i<4; i++)
		encoded[offset+i] = rand();

	offset += 4;

	for(i=0; i < length; i++)
		encoded[offset+i] = msg[i] ^ encoded[offset-4+(i%4)];
	
	encoded[offset + length] = 0;

	int result = SSL_write(wssSock, encoded, length + offset);
	pthread_mutex_unlock(&(apiObj->sslLock));
	free(encoded);

	return result;
}

static void* hbThreadFunc(){
	for(;;){
//		while(!hbInterval)
//			usleep(50000);

//		while(wsSendMsg("{\"op\":1,\"d\":null}") > 0)
//			usleep(hbInterval * 1000);

		sleep(1);
	}

	return 0;
}

static void* sockThreadFunc(){
//	struct pollfd pfd;
//	char *msg = NULL;
//	unsigned char opcode;

	for(;;){
/*		while(!hbInterval)
			usleep(50000);

		pfd.fd = wsSock;
		pfd.events = POLLIN;

		while(SSL_pending(wssSock) || poll(&pfd, 1, -1) > 0){
			if(!hbInterval || !(pfd.revents & POLLIN))
				break;

			wsReadMsg(&msg, &opcode);

			pthread_mutex_lock(&(apiObj->wshLock));
			struct wshNode *node = wshRoot;
			while(node != NULL){
				node->func(msg, opcode);
				node = node->next;
			}
			pthread_mutex_unlock(&(apiObj->wshLock));

			free(msg);
		}*/

		sleep(1);
	}

	return 0;
}

unsigned char openSocks(char *hostname, int *sock, SSL **ssock, SSL_CTX **sCTX, unsigned char blocking){
	struct addrinfo sockaddr, *servinfo, *p;
	int rv;

	memset(&sockaddr, 0, sizeof sockaddr);
	sockaddr.ai_family = AF_INET;
	sockaddr.ai_socktype = SOCK_STREAM;

	if((rv = getaddrinfo(hostname, "443", &sockaddr, &servinfo)) != 0){
		printf("getaddrinfo for %s failed! %s\n", hostname,  gai_strerror(rv));
		return 0;
	}

	for(p = servinfo; p != NULL; p = p->ai_next){
		if((*sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
			printf("Nope\n");
			continue;
		}

		if(connect(*sock, p->ai_addr, p->ai_addrlen) == -1){
			printf("Connection failed\n");
			close(*sock);
			*sock = 0; 
			continue;
		}

		break;
	}

	freeaddrinfo(servinfo);

	if(!blocking)
		fcntl(*sock, F_SETFL, fcntl(*sock, F_GETFL, 0) | O_NONBLOCK);

	if(p == NULL || !*sock){
		printf("Socket creation for %s failed!\n", hostname);
		return 0;
	}
 
	pthread_mutex_lock(&(apiObj->sslLock));

	*sCTX = SSL_CTX_new(TLSv1_2_client_method());
	*ssock = SSL_new(*sCTX);

	SSL_set_fd(*ssock, *sock);

	ERR_clear_error();

	int status;
	while((status = SSL_connect(*ssock)) < 1){
		if(!blocking && status == -1){
			int sslerr = SSL_get_error(*ssock, status);
			struct pollfd pfd;

			pfd.fd = *sock;
			pfd.events = POLLIN | POLLOUT;

			if((sslerr == SSL_ERROR_WANT_READ || sslerr == SSL_ERROR_WANT_WRITE) && poll(&pfd, 1, -1) > 0 && (pfd.revents & POLLIN || pfd.revents & POLLOUT))
				continue;
		}

		printf("SSL Connection failed: %i\n%i %i\n", -1, SSL_get_error(*ssock, -1), errno);
		printf("Error string: %s\n", ERR_error_string(ERR_get_error(), NULL));

		*ssock = NULL;
		SSL_CTX_free(*sCTX);
		close(*sock);

		pthread_mutex_unlock(&(apiObj->sslLock));
		return 0;
	}

	pthread_mutex_unlock(&(apiObj->sslLock));
	return 1;
}

void closeSocks(int *sock, SSL **ssock, SSL_CTX **sCTX){
	pthread_mutex_lock(&(apiObj->sslLock));

	if(*ssock != NULL){
		while(SSL_shutdown(*ssock) == 0){}
		SSL_free(*ssock);
	}
	
	if(*sCTX != NULL)
		SSL_CTX_free(*sCTX);

	*ssock = NULL;
	*sCTX = NULL;

	pthread_mutex_unlock(&(apiObj->sslLock));

	if(*sock != 0)
		close(*sock);

	*sock = 0;
}

void connectToDiscord(struct identityNode *node){
	if(node->wssSock != NULL){
		printf("You are already connected\n");
		return;
	}

	printf("Opening websocket...\n");

	// Let's declare some stuff
	char buf[4096] = {0};
	int httpSock, retlen;

	SSL *httpsSock = NULL;
	SSL_CTX *httpsCTX = NULL;

	if(!openSocks("discordapp.com", &httpSock, &httpsSock, &httpsCTX, 1)){
		printf("Unable to open connection to the Discord gateway server\n");
		return;
	}

	sprintf(buf, "GET /api/v6/gateway HTTP/1.1\r\nHost: discordapp.com\r\n\r\n");

	if(SSL_write_lock(httpsSock, buf, strlen(buf)) < 1){
		printf("Failed to send request to gateway server\n");
		closeSocks(&httpSock, &httpsSock, &httpsCTX);
		return;
	}

	retlen = SSL_read(httpsSock, buf, 4095);

	closeSocks(&httpSock, &httpsSock, &httpsCTX);

	buf[retlen] = 0;
	char *tmp = strstr(buf, "\r\n\r\n") + 19;
	retlen = strchr(tmp, '"') - tmp;
	char *hostname = malloc(retlen + 1);
	strncpy(hostname, tmp, retlen);
	hostname[retlen] = 0;

	if(node->type == IDENTITY_NODE_TYPE_SELF){
		printf("Self bot auth stuff here\n");
/*	struct curl_slist *headers = NULL;

	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();

	if(!curl){
		printf("CURL error!\n");
		return;
	}

	char *postStr = malloc(strlen(EMAIL) + strlen(PASS) + 27);
	sprintf(postStr, "{\"email\":\"%s\",\"password\":\"%s\"}", EMAIL, PASS);

	headers = curl_slist_append(headers, "Content-Type: application/json");
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_URL, "https://discordapp.com/api/v6/auth/login");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postStr);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlAuthCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);

	res = curl_easy_perform(curl);
	if(res != CURLE_OK){
		fprintf(stderr, "curl_easy_perform() failed: %s\n\n", curl_easy_strerror(res));	
		return;
	}

	curl_slist_free_all(headers);
	free(postStr);

	curl_easy_cleanup(curl);
	curl_global_cleanup();
	
	char token[60] = "authentication token";
	*/
	}

	// Now to open the websocket connection
	if(!openSocks(hostname, &(node->wsSock), &(node->wssSock), &(node->wssCTX), 0)){
		free(hostname);
		printf("Failed to open websocket connection\n");
		return;
	}

	// Now that we've opened the socket, we have to send some HTTP headers to tell Discord to upgrade this socket to a web socket
	sprintf(buf, "GET /?encoding=json&v=6 HTTP/1.1\r\nHost: %s\r\nUser-Agent: :sunglasses:\r\nUpgrade: WebSocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: c4TgAagMMXif7swkYTcXSw==\r\nSec-WebSocket-Version: 13\r\n\r\n", hostname);
	// Since the socket is non-blocking, I should probably actually add something here to account for that in case the write fails...
	retlen = SSL_write_lock(node->wssSock, buf, strlen(buf));

	free(hostname);

	if(retlen < 1){
		printf("Failed to send HTTP handshake headers\n");
		closeSocks(&(node->wsSock), &(node->wssSock), &(node->wssCTX));
		return;
	}

	// Now we wait for a response. We should get some headers saying the connection was upgraded and a "sec-websocket-accept" header we're supposed to parse and verify, but fuck that. Discord works and SHA1 is just more overhead
	struct pollfd pfd;
	pfd.fd = node->wsSock;
	pfd.events = POLLIN;

	if(poll(&pfd, 1, -1) < 1 || !(pfd.revents & POLLIN) || (retlen = SSL_read(node->wssSock, buf, 4095)) < 1){
		printf("Failed to read HTTP handshake response headers\n");
		closeSocks(&(node->wsSock), &(node->wssSock), &(node->wssCTX));
		return;
	}

	buf[retlen] = 0;

	// We should check to make sure the connection was upgraded though at least...
	if(strncmp(buf, "HTTP/1.1 101 ", 13) != 0){
		printf("The web socket connection failed to upgrade\n");
		closeSocks(&(node->wsSock), &(node->wssSock), &(node->wssCTX));
		return;
	}


	// Now to actually start dealing with Discord...
	unsigned char opcode;
	char *msg = NULL;

	// Read the "Hello" payload and get the heartbeat interval
	if(poll(&pfd, 1, -1) < 1 || !(pfd.revents & POLLIN)){
		printf("Hello failed...?\n");
		closeSocks(&(node->wsSock), &(node->wssSock), &(node->wssCTX));
		return;
	}

	wsReadMsg(node->wssSock, &msg, &opcode);
	node->hbInterval = strtoul(strstr(msg, "\"heartbeat_interval\":") + 21, NULL, 10);
	free(msg);

	// Send identify... (I don't think I'll ever need to JSONEscape the token)
	sprintf(buf, "{\"op\":2,\"d\":{\"token\":\"%s\",\"properties\":{\"$os\":\":sunglasses:\",\"$browser\":\":sunglasses:\",\"$device\":\":sunglasses:\"},\"compress\":false,\"large_threshold\":50,\"shards\":[0,1]}}", node->token);
	if(wsSendMsg(node->wssSock, buf) < 1){
		printf("Identification failed?\n");
		return;
	}

	if(poll(&pfd, 1, -1) < 1 || !(pfd.revents & POLLIN)){
		printf("No response to identification...?\n");
		return;
	}

	wsReadMsg(node->wssSock, &msg, &opcode);

	printf("msg: %s\n", msg);

	printf("Websocket opened\n");
}
