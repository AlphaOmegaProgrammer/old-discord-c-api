enum getChannelMessagesIndexMode{
	GET_CHANNEL_MESSAGES_INDEX_MODE_NONE,
	GET_CHANNEL_MESSAGES_INDEX_MODE_AROUND,
	GET_CHANNEL_MESSAGES_INDEX_MODE_BEFORE,
	GET_CHANNEL_MESSAGES_INDEX_MODE_AFTER
};

struct apiObj{
	char *urlHost;
	char *urlPath;
	char *requiredHeaders;

	pthread_mutex_t sslLock;
	pthread_mutex_t iLock;

	pthread_mutex_t cLock;
	void (*addCommand)(char*, unsigned char, char*, void*);
	void (*delCommand)(char*);

	pthread_mutex_t wshLock;
	void (*addWSH)(char*, void*);
	void (*delWSH)(char*);

	void (*getChannel)(struct apiObj*, char*);
	void (*modifyChannel)(struct apiObj*, char*, char*, char*);
	void (*deleteChannel)(struct apiObj*, char*);
	void (*getChannelMessages)(struct apiObj*, char*, unsigned char, unsigned char, char*);
	void (*createMessage)(struct apiObj*, char*, char*);
};

struct apiObj *apiObj = NULL;
