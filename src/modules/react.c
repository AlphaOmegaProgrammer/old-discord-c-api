#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../apiObj.h"

void wsHook(char *msg, unsigned char opcode){
	if(opcode == 11)
		return;

	if(opcode != 0){
		printf("OPCODE: %hhu!\n", opcode);
		return;
	}

	char *start = strstr(msg, "\"t\":\"") + 5;
	char *end = strstr(start, "\"");
	char *type = malloc(end-start+1);

	memcpy(type, start, end-start);
	type[end-start] = 0;

	if(strcmp(type, "TYPING_START") != 0){
		free(type);
		return;
	}

	free(type);

	start = strstr(msg, "\"channel_id\":\"") + 14;
	end = strstr(start, "\"");
	char *channelId = malloc(end-start+1);
	memcpy(channelId, start, end-start);
	channelId[end-start] = 0;

	apiObj->createMessage(apiObj, channelId, "You are typing!");

	free(channelId);
}

void onOpenFunc(struct apiObj *apiObjP){
	apiObj = apiObjP;

	apiObj->addWSH("Ready", (void*)wsHook);
}

void onCloseFunc(){
	apiObj->delWSH("Ready");
}
