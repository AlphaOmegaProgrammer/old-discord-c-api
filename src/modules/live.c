#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../apiObj.h"

void liveCallback(char* msg){
	msg += 5;

	if(strncmp(msg, "getChannel ", 11) == 0)
		apiObj->getChannel(apiObj, msg + 11);

	else if(strncmp(msg, "modifyChannel ", 14) == 0){
		char *channelId = msg + 14;
		char *key = strchr(channelId, ' ');
		key[0] = 0;
		key++;

		char *val = strchr(key, ' ');
		val[0] = 0;
		val++;

		apiObj->modifyChannel(apiObj, channelId, key, val);

	}else if(strncmp(msg, "deleteChannel ", 14) == 0)
		apiObj->deleteChannel(apiObj, msg + 14);

	else if(strncmp(msg, "getChannelMessages ", 19) == 0){
		apiObj->getChannelMessages(apiObj, msg + 19, 100, GET_CHANNEL_MESSAGES_INDEX_MODE_NONE, NULL);

	}else if(strncmp(msg, "createMessage ", 14) == 0){
		char *channelId = msg + 14;
		char *message = strchr(channelId, ' ');
		message[0] = 0;
		message++;

		apiObj->createMessage(apiObj, channelId, message);

	}
}

void onOpenFunc(struct apiObj *apiObjP){
	apiObj = apiObjP;

	apiObj->addCommand("live ", 5, "Allows you to call functions directly from the API Object.", liveCallback);
}

void onCloseFunc(){
	apiObj->delCommand("live ");
}
