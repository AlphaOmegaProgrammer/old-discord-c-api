all: clean modules compile
debug: clean mdebug cdebug

clean:
	-rm bin/*

modules:
	for f in $(wildcard src/modules/*.c); do gcc -xc -Wall -Wextra -Werror -O2 -march=native -fstack-protector-all -shared -nostartfiles -nostdlib -o bin/`basename $${f%.c}`.so -fPIC $$f; done

compile:
	gcc -xc -Wall -Wextra -Werror -pedantic -O2 -fstack-protector-all -march=native -o bin/discord-bot -pthread src/main.c -lcrypto -ldl -lssl


mdebug:
	for f in $(wildcard src/modules/*.c); do gcc -g -xc -Wall -Wextra -Werror -O2 -march=native -fstack-protector-all -shared -nostartfiles -nostdlib -o bin/`basename $${f%.c}`.so -fPIC $$f; done

cdebug:
	gcc -g -xc -Wall -Wextra -Werror -pedantic -O2 -fstack-protector-all -march=native -o bin/discord-bot -pthread src/main.c -lcrypto -ldl -lssl
